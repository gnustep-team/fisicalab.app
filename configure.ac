#                                               -*- Autoconf -*-
# Process this file with autoconf to produce a configure script.

AC_CONDITIONAL
AC_PREREQ([2.68])
AC_INIT(fisicalab, 0.4.0)
#AC_CANONICAL_HOST

# Check if we are in Mac OS
#build_mac=no

#case "${host_os}" in
#    darwin*)
#        build_mac=yes
#        ;;
#esac

# Pass the conditionals to automake
#AM_CONDITIONAL([OSX], [test "$build_mac" = "yes"])

# If GNUSTEP_MAKEFILES is undefined, try to use gnustep-config to determine it.
if test -z "$GNUSTEP_MAKEFILES"; then
  GNUSTEP_MAKEFILES=`gnustep-config --variable=GNUSTEP_MAKEFILES 2>&5`
fi

if test -z "$GNUSTEP_MAKEFILES"; then
  AC_MSG_ERROR([You must have the gnustep-make package installed and set up the GNUSTEP_MAKEFILES environment variable to contain the path to the makefiles directory before configuring!])
fi

AC_CONFIG_AUX_DIR([$GNUSTEP_MAKEFILES])

OBJC_FLAGS=`gnustep-config --objc-flags 2>&5`
GSL_PREFIX=`gsl-config --prefix 2>&5`
GNUSTEP_LOCAL_LIBRARIES=`gnustep-config --variable=GNUSTEP_LOCAL_LIBRARIES 2>&5`
GNUSTEP_SYSTEM_LIBRARIES=`gnustep-config --variable=GNUSTEP_SYSTEM_LIBRARIES 2>&5`

CPPFLAGS="$CPPFLAGS -x objective-c $OBJC_FLAGS"
LDFLAGS="$LDFLAGS -L$GSL_PREFIX/lib -L$GNUSTEP_LOCAL_LIBRARIES -L$GNUSTEP_SYSTEM_LIBRARIES"

# Checks for programs.
AC_PROG_CC

# Chech for GUI option
build_mac=no
IUP=no
IUPLIB=yes
RENAISSANCE=no
LIBREN=yes

AC_ARG_WITH([iup],
            [AS_HELP_STRING([--with-iup],
              [build the interface using IUP @<:@default=no@:>@])],
            [IUP=yes],
            [])

AC_ARG_WITH([renaissance],
            [AS_HELP_STRING([--with-renaissance],
              [build the interface using renaissance @<:@default=no@:>@])],
            [RENAISSANCE=yes],
            [])

AC_ARG_WITH([cocoa],
            [AS_HELP_STRING([--with-cocoa],
              [build the interface using MacOS Cocoa @<:@default=no@:>@])],
            [build_mac=yes],
            [])

# Pass the conditionals to automake
AM_CONDITIONAL([MACOS], [test "$build_mac" = "yes"])

if test $RENAISSANCE = yes; then
   AC_SUBST(IUP,"no")
fi

if test $build_mac = no; then
# Checks for libraries.
BASE=yes
GUI=yes
GSL=yes

AC_CHECK_LIB(gnustep-base,main,[],BASE=no)

if test $RENAISSANCE = yes; then
    AC_SUBST(GORM,[])
    AC_SUBST(MAIN_NIB,[])
    AC_SUBST(GSMARKUP, "Resources/firstLaunchPanel.gsmarkup Resources/menu-gs.gsmarkup Resources/fisica.gsmarkup Resources/preferences.gsmarkup")
    AC_SUBST(RENAISSANCE_INTERFACE,"RenaissanceController.h ChalkboardView.h ObjectData.h InfoTable.h")
    AC_SUBST(RENAISSANCE_IMPLEMENTATION, "RenaissanceController.m ChalkboardView.m ObjectData.m InfoTable.m")
    #AC_CHECK_LIB(Renaissance,main,[],LIBREN=no)
    AC_SUBST(LOCALIZED,"logoPanel.tif")
    AC_SUBST(IUPUI,[])
    AC_SUBST(RENUI,["-D RENUI"])
    AC_SUBST(MACOS,[])
else

   if test $IUP = yes; then
      AC_CHECK_LIB(iup,main,[],IUPLIB=no)
      AC_CHECK_LIB(iupim,main,[],IUPLIB=no)
      AC_CHECK_LIB(iupcontrols,main,[],IUPCONTROLs=no)
      AC_CHECK_LIB(im,main,[],IUPLIB=no)
      AC_SUBST(GORM,[])
      AC_SUBST(MAIN_NIB,[])
      AC_SUBST(GSMARKUP, [])
      AC_SUBST(IUP_INTERFACE,IupInterface.h)
      AC_SUBST(IUP_IMPLEMENTATION,IupInterface.m)
      AC_SUBST(LOCALIZED,"logoPanel.tif")
      AC_SUBST(IUPUI,"-D IUPUI")
      AC_SUBST(RENUI,[])
      AC_SUBST(MACOS,[])
   else
      AC_CHECK_LIB(gnustep-gui,main,[],GUI=no)
      AC_SUBST(GORM,"Fisica.gorm preferences.gorm groups.gorm properties.gorm firstLaunchPanel.gorm staticRigidBodies.gorm dynamicsCircular.gorm thermodynamics.gorm")
      AC_SUBST(MAIN_NIB,"NSMainNibFile = Fisica.gorm;")
      AC_SUBST(GSMARKUP, [])
      AC_SUBST(IUP_INTERFACE,"AppController.h ChalkboardView.h ObjectData.h InfoTable.h")
      AC_SUBST(IUP_IMPLEMENTATION,"AppController.m ChalkboardView.m ObjectData.m InfoTable.m")
      AC_SUBST(IUPUI,[])
      AC_SUBST(RENUI,[])
      AC_SUBST(MACOS,[])
   fi
fi

# Checks for libs.
if test $BASE = no; then
   AC_MSG_ERROR([`fisicalab requires the library gnustep-base.']);
fi

if test $IUP = yes; then
   if test $IUPLIB = no; then
      AC_MSG_ERROR([`fisicalab requires the library IUP.']);
   fi
else
   if test $RENAISSANCE = yes; then

      if test $LIBREN = no; then
         AC_MSG_ERROR([`fisicalab requires the library Renaissance.']);
      fi
      
   else
      if test $GUI = no; then
         AC_MSG_ERROR([`fisicalab requires the library gnustep-gui.']);
      else
         GUI_VERSION=`grep --recursive --no-filename GSBundleShortVersionString /usr/GNUstep/Local/Library/Libraries/gnustep-gui/Versions/`
         GUI_VERSION=${GUI_VERSION#*.}
         GUI_VERSION=${GUI_VERSION%.*}

         if test [$GUI_VERSION -lt 23]; then
            AC_MSG_ERROR([`fisicalab requires at least gnustep-gui 0.23.0.']);
         fi
      fi
   fi
fi

# Checks for header files.
AC_CHECK_HEADER([Foundation/Foundation.h], [],
[AC_MSG_ERROR([You should install the header files for pachage gnustep-base.])])

if test $IUP = yes; then
   AC_CHECK_HEADER([iup/iup.h], [],
   [AC_MSG_ERROR([You should install the header files for pachage IUP.])])
else
   AC_CHECK_HEADER([AppKit/AppKit.h], [],
   [AC_MSG_ERROR([You should install the header files for pachage gnustep-gui.])])
   
   if test $RENAISSANCE = yes; then
      AC_CHECK_HEADER([Renaissance/Renaissance.h], [],
      [AC_MSG_ERROR([You should install the header files for pachage Renaissance.])])
   fi

fi

else
BASE=yes

      AC_CHECK_LIB(gnustep-baseadd,main,[],BASE=no)
      AC_SUBST(GORM,[])
      AC_SUBST(MAIN_NIB,[])
      AC_SUBST(GSMARKUP, "Resources/firstLaunchPanel.gsmarkup Resources/menu.gsmarkup Resources/fisica.gsmarkup Resources/preferences.gsmarkup")
      AC_SUBST(RENAISSANCE_INTERFACE, "RenaissanceController.h ChalkboardView.h ObjectData.h InfoTable.h AddsNSString.h")
      AC_SUBST(RENAISSANCE_IMPLEMENTATION, "RenaissanceController.m ChalkboardView.m ObjectData.m InfoTable.m AddsNSString.m")
      #AC_CHECK_LIB(Renaissance,main,[],LIBREN=no)
      AC_CHECK_HEADER([Renaissance/Renaissance.h], [],
         [AC_MSG_ERROR([You should install the header files for pachage Renaissance.])])
      AC_SUBST(LOCALIZED,"logoPanel.tif")
      AC_SUBST(IUPUI,[])
      AC_SUBST(RENUI,["-D RENUI"])
      AC_SUBST(MACOS,["-D MACOS"])
fi

# Checks for libs.
if test $BASE = no; then
   AC_MSG_ERROR([`fisicalab requires the library gnustep-baseadd.']);
fi

AC_CHECK_LIB(m,main,[],GSL=no)
AC_CHECK_LIB(gslcblas,main,[],GSL=no)
AC_CHECK_LIB(gsl,main,[],GSL=no)

if test $GSL = no; then
   AC_MSG_ERROR([`fisicalab requires the library gsl.']);
fi

AC_CHECK_HEADERS([gsl/gsl_vector.h gsl/gsl_multiroots.h gsl/gsl_rng.h], [],
[AC_MSG_ERROR([You should install the header files for pachage gsl.])])

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_SIZE_T

# Checks for library functions.
AC_CHECK_FUNCS([floor sqrt])

AC_CONFIG_FILES([GNUmakefile GNUmakefile.preamble FisicaLabInfo.plist])
AC_OUTPUT
