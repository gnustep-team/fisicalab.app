/* 
   Copyright (C) 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016 German A. Arias

   This file is part of FísicaLab application

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
 
   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
 
   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/
 
#import <AppKit/AppKit.h>

@interface AppController : NSObject
{
  // Outlets
  id elements;
  id helpPanel;
  id unitsSelector;
  id system;
  id chalkboard;
  id info;
  id groupsScrollview;
  id groups;
  id staticRigidBodies;
  id staticRigidBodiesView;
  id dynamicsCircularParticles;
  id dynamicsCircularParticlesView;
  id thermodynamics;
  id thermodynamicsView;

  NSPanel *preferencesPanel;
  // Preferences panel outlets
  id widthLabel;
  id heightLabel;
  id fontsizeLabel;
  id widthStepper;
  id heightStepper;
  id fontsizeStepper;

  NSPanel *firstLaunchPanel;
  NSWindow *propertiesWindow;
}

- (id) init;
- (void) applicationWillFinishLaunching: (NSNotification *)aNotification;
- (void) applicationDidFinishLaunching: (NSNotification*)aNotification;

- (void) showPrefPanel: (id)sender;
- (void) showPropertiesWindow: (id)sender;
- (void) selectGroup: (id)sender;

- (void) addToChalkboard: (id)sender;

- (void) windowWillClose: (NSNotification *)aNotification;

// Save and load documents
- (void) saveProblem: (id) sender;

// Preferences
- (void) changeChalkboardWidth: (id)sender;
- (void) changeChalkboardHeight: (id)sender;
- (void) changeFontsizeTooltips: (id)sender;
- (void) restoreDefaults: (id)sender;

// First launch panel
- (void) notShowAgain: (id)sender;

@end
