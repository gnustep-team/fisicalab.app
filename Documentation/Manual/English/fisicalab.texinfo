\input texinfo   @c -*-texinfo-*-
@c This is part of the FisicaLab User Manual.
@c Copyright (C)  2013-2023 German A. Arias.
@c See the file copying.texinfo for copying conditions.

@c %**start of header
@setfilename fisicalab.info
@documentlanguage en
@documentencoding UTF-8
@settitle GNU F@'{@dotless{i}}sicaLab
@smallbook
@c %**end of header

@copying
Copyright @copyright{} 2009, 2010, 2012, 2013, 2014, 2020, 2023 Germ@'an A. Arias.

    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".
@end copying

@titlepage
@title GNU F@'{@dotless{i}}sicaLab
@subtitle The free software to the physics (free as in fredom).
@subtitle User manual for version 0.4.0, first edition.
@sp 4
@image{fisicalab_logo, 6cm}
@author Germ@'an A. Arias

@c The following two commands start the copyright page.
@page
@vskip 0pt plus 1filll
@insertcopying
@end titlepage
     
@c Output the table of contents at the beginning.
@contents

@ifnottex
@node Top, Welcome, (dir), (dir)
@top Contenido

FisicaLab is an educational application to solve physics problems. Its main objective is let the user to focus in physics concepts, leaving aside the mathematical details (FisicaLab take care of them). This allows the user to become familiar with the physical concepts without running the risk of getting lost in mathematical details. And so, when the user gain confidence in applying physical concepts, will be better prepared to solve the problems by hand (with pen and paper). FisicaLab is easy to use and very intuitive. However, in order to take advantage of all its features, we recommend you read first these help files.


@menu
* Welcome::                     
* Kinematics::                  
* Examples kinematics of particles::  
* Circular kinematics::         
* Examples circular kinematics of particles::  
* Statics::                     
* Examples statics::            
* Rigid statics::               
* Examples statics rigid bodies::  
* Dynamics::                    
* Examples dynamics of particles::  
* Circular dynamics::           
* Examples circular dynamics::  
* Calorimetry::                 
* Examples calorimetry::
* Thermodynamics::
* Examples thermodynamics::
* GFDL::                        
* Indice::                      

@detailmenu
 --- The Detailed Node Listing ---

Introduction

* Handling the elements::       
* Element data::                
* How it works::                
* Messages::                    

Module kinematics of particles

* Stationary reference system:                          Stationary reference system (k).  
* Mobile reference system:                              Mobile reference system (k).  
* Mobile reference system in X/Y:                       Mobile reference system in X/Y (k).  
* Mobile:                                               Mobile (k).                  
* Mobile in X/Y:                                        Mobile in X/Y (k).           
* Mobile in X/Y with constant velocity:                 Mobile in X/Y with constant velocity (k).
* Cannon:                                               Cannon (k).                  
* Mobile radial:                                        Mobile radial (k).           
* Distance:                                             Distance (k).                
* Distance XY:                                          Distance XY (k).             
* Point:                                                Point (k).                   
* Relative velocity:                                    Relative velocity (k).       

Examples kinematics of particles

* Example 1 (k)::               
* Example 2 (k)::               
* Example 3 (k)::               
* Example 4 (k)::               
* Example 5 (k)::               
* Example 6 (k)::               
* Example 7 (k)::               

Module circular kinematics of particles

* Stationary reference system:                          Stationary reference system (ck).  
* Mobile with circular motion:                          Mobile circular (ck).        
* Mobile with polar circular motion:                    Mobile polar (ck).           
* Angular velocity:                                     Angular velocity (ck).       
* Angular acceleration:                                 Angular acceleration (ck).   
* Total acceleration:                                   Total acceleration (ck).     
* Frequency:                                            Frequency (ck).              
* Period:                                               Period (ck).                 
* Number of laps:                                       Number of laps (ck).         
* Center of rotation:                                   Center of rotation (ck).     
* Distance:                                             Distance (ck).               
* Arc length:                                           Arc length (ck).             
* Coordinate:                                           Coordinate (ck).             
* Relative velocity:                                    Relative velocity (ck).      

Examples circular kinematics of particles

* Example 1 (ck)::              
* Example 2 (ck)::              
* Example 3 (ck)::              
* Example 4 (ck)::              
* Example 5 (ck)::              

Module statics of particles

* Stationary reference system:                          Stationary reference system (s).  
* Block:                                                Block (s).                   
* Block above an inclined plane to the left:            Block left (s).              
* Block above an inclined plane to the right:           Block right (s).             
* Springs:                                              Springs (s).                 
* Pulley:                                               Pulley (s).                  
* Static point:                                         Static point (s).            
* Angles:                                               Angles (s).                  
* Forces:                                               Forces (s).                  
* Frictions:                                            Frictions (s).               
* Vertical/Horizontal resultant:                        V/H resultant (s).           
* Resultant:                                            Resultant (s).               

Examples statics

* Example 1 (s)::               
* Example 2 (s)::               
* Example 3 (s)::               
* Example 4 (s)::               
* Example 5 (s)::               
* Example 6 (s)::               
* Example 7 (s)::               
* Example 8 (s)::               
* Example 9 (s)::               

Module rigid statics

* Stationary reference system:                                  Stationary reference system (rs).  
* Points:                                                       Points (rs).                 
* Beam:                                                         Beam (rs).                   
* Solid:                                                        Solid (rs).                  
* Point:                                                        Point (rs).                  
* Angles:                                                       Angles (rs).                 
* Elements of beam:                                             Elements of beam (rs).       
* Elements of solid:                                            Elements of solid (rs).      
* Forces:                                                       Forces (rs).                 
* Frictions:                                                    Frictions (rs).              
* Couple:                                                       Couple (rs).                 
* Beams of 2 forces:                                            Beams of 2 forces (rs).      
* Truss:                                                        Truss (rs).                  
* Joint:                                                        Joint (rs).                  
* Beams of truss:                                               Beams of truss (rs).         
* Resultant:                                                    Resultant (rs).              
* Resultant with horizontal force:                              Resultant horizontal (rs).  
* Resultant with vertical force:                                Resultant vertical (rs).  

Examples statics rigid bodies

* Example 1 (rs)::              
* Example 2 (rs)::              
* Example 3 (rs)::              
* Example 4 (rs)::              
* Example 5 (rs)::              
* Example 6 (rs)::              
* Example 7 (rs)::              
* Example 8 (rs)::              
* Example 9 (rs)::              
* Example 10 (rs)::             
* Example 11 (rs)::             
* Example 12 (rs)::             
* Example 13 (rs)::             

Module dynamics of particles

* Stationary reference system:                                     Stationary reference system (d).
* Mobile:                                                          Mobile (d).                  
* Mobile in X/Y:                                                   Mobile in X/Y (d).           
* Block with vertical movement:                                    Block vertical (d).          
* Block with horizontal movement:                                  Block horizontal (d).        
* Block with movement along an inclined plane to the left:         Block plane to the left (d).  
* Block with movement along an inclined plane to the right:        Block plane to the right (d).  
* Pulley:                                                          Pulley (d).                  
* Springs:                                                         Springs (d).                 
* Forces:                                                          Forces (d).                  
* Frictions:                                                       Frictions (d).               
* Frictions between blocks (contacts):                             Frictions between blocks (d).  
* Relation between accelerations:                                  Relation between accelerations (d).  
* Relative motion:                                                 Relative motion (d).         
* Collision:                                                       Collision (d).               
* Energy:                                                          Energy (d).                  
* Momentum:                                                        Momentum (d).                
* Power:                                                           Power (d).                   

Examples dynamics of particles

* Example 1 (d)::               
* Example 2 (d)::               
* Example 3 (d)::               
* Example 4 (d)::               
* Example 5 (d)::               
* Example 6 (d)::               
* Example 7 (d)::               
* Example 8 (d)::               
* Example 9 (d)::               
* Example 10 (d)::              
* Example 11 (d)::              
* Example 12 (d)::              

Module circular dynamics of particles

* Stationary reference system:                                  Stationary reference system (cd).  
* Object in rest:                                               Object in rest (cd).         
* Mobile with linear movement:                                  Mobile linear (cd).          
* Mobile with circular movement:                                Mobile circular (cd).        
* Mobile with polar circular movement:                          Mobile polar (cd).           
* Mobile with perpendicular circular movement:                  Mobile perpendicular (cd).   
* Angular velocity:                                             Angular velocity (cd).       
* Centripetal acceleration:                                     Centripetal acceleration (cd).  
* Angular acceleration:                                         Angular acceleration (cd).   
* Energy:                                                       Energy (cd).                 
* Angular momentum:                                             Angular momentum (cd).       
* Linear momentum:                                              Linear momentum (cd).        
* Power:                                                        Power (cd).                  
* Initial System:                                               Initial System (cd).         
* Final System:                                                 Final System (cd).           
* Center of rotation:                                           Center of rotation (cd).     
* Springs:                                                      Springs (cd).                
* Forces:                                                       Forces (cd).                 
* Frictions:                                                    Frictions (cd).              
* Angles:                                                       Angles (cd).                 
* Moment of a force or couple of forces:                        Moment (cd).                 
* Total acceleration (Triangle of accelerations):               Total acceleration (cd).     
* Maximum acceleration:                                         Maximum acceleration (cd).   
* Inertia:                                                      Inertia (cd).                
* Absolute velocity:                                            Absolute velocity (cd).      
* Sine of angle:                                                Sine of angle (cd).          
* Supported combinations of elements Energy, Angular Momentum, Linear Momentum and Power:    Combinations (cd).           

Examples circular dynamics of particles

* Example 1 (cd)::              
* Example 2 (cd)::              
* Example 3 (cd)::              
* Example 4 (cd)::              
* Example 5 (cd)::              
* Example 6 (cd)::              
* Example 7 (cd)::              
* Example 8 (cd)::              
* Example 9 (cd)::              
* Example 10 (cd)::             
* Example 11 (cd)::             
* Example 12 (cd)::             
* Example 13 (cd)::             

Calorimetry

* Laboratory clock:                                             Laboratory clock (cal).      
* Applied heat:                                                 Applied heat (cal).          
* Applied heat flow:                                            Applied heat flow (cal).     
* Heat extracted:                                               Heat extracted (cal).        
* Refrigeration:                                                Refrigeration (cal).         
* Block:                                                        Block (cal).                 
* Liquid:                                                       Liquid (cal).                
* Gas:                                                          Gas (cal).                   
* Linear expansion:                                             Linear expansion (cal).      
* Superficial expansion:                                        Superficial expansion (cal).  
* Volumetric expansion:                                         Volumetric expansion (cal).  
* Change of state solid-liquid:                                 Change solid-liquid (cal).  
* Change of state liquid-gas:                                   Change liquid-gas (cal).  
* Process:                                                      Process (cal).               
* Calorimeter:                                                  Calorimeter (cal).           
* Gas at constant pressure:                                     Gas constant pressure (cal).  
* Gas at constant temperature:                                  Gas constant temperature (cal).  
* Gas at constant volume:                                       Gas constant volume (cal).  
* Gas PV/T:                                                     Gas PV/T (cal).                   
* Heat exchanger:                                               Heat exchanger (cal).        

Examples calorimetry

* Example 1 (cal)::             
* Example 2 (cal)::             
* Example 3 (cal)::             
* Example 4 (cal)::             
* Example 5 (cal)::             
* Example 6 (cal)::             
* Example 7 (cal)::             
* Example 8 (cal)::             
* Example 9 (cal)::             
* Example 10 (cal)::            
* Example 11 (cal)::            
* Example 12 (cal)::            
* Example 13 (cal)::

Thermodynamics

* Applied heat:                                                 Applied heat (therm).          
* Work:                                                         Work (therm).
* Heat extracted:                                               Heat extracted (therm).        
* Adiabatic process:                                            Adiabatic process (therm).         
* Isochoric process:                                            Isochoric process (therm).                 
* Isothermal process:                                           Isothermal process (therm).    
* Isobaric process:                                             Isobaric process (therm).
* Sensible heat:                                                Sensible heat (therm).                   
* Vaporization heat:                                            Vaporization heat (therm).      
* Heat engine:                                                  Heat engine (therm).  
* Refrigerator:                                                 Refrigerator (therm).  
* Heat pump:                                                    Heat pump (therm).
* Gas PV/T:                                                     Gas PV/T (therm).

Examples thermodynamics

* Example 1 (thermo)::             
* Example 2 (thermo)::             
* Example 3 (thermo)::             
* Example 4 (thermo)::             
* Example 5 (thermo):: 

@end detailmenu
@end menu

@end ifnottex

@include introduction.texinfo
@include kinematics.texinfo
@include kinematics-examples.texinfo
@include kinematics-circular.texinfo
@include kinematics-circular-examples.texinfo
@include statics.texinfo
@include statics-examples.texinfo
@include statics-rigid.texinfo
@include statics-rigid-examples.texinfo
@include dynamics.texinfo
@include dynamics-examples.texinfo
@include dynamics-circular.texinfo
@include dynamics-circular-examples.texinfo
@include calorimetry.texinfo
@include calorimetry-examples.texinfo
@include thermodynamics.texinfo
@include thermodynamics-examples.texinfo
@include copying.texinfo
@include concept-index.texinfo

@bye
