@c -*-texinfo-*-
@c This is part of the FisicaLab User Manual.
@c Copyright (C)  2013 German A. Arias.
@c See the file copying.texinfo for copying conditions.

@node Circular kinematics, Examples circular kinematics of particles, Examples kinematics of particles, Top
@chapter Module circular kinematics of particles

@cindex Module circular kinematics of particles

The conversion factors for SI system are:

@sp 2

@table @strong

@item km
kilometer

@item cm
centimeter

@item mm
millimeter

@item mi
miles

@item ft
feet

@item in
inch

@item h
hour

@item min
minute

@item km/h
kilometer per hour

@item cm/s
centimeter per second

@item mm/s
millimeter per second

@item mph
miles per hour

@item ft/s
feet per second

@item in/s
inch per second

@item kt
knot

@item cm/s2
centimeters per squared second

@item mm/s2
millimeters per squared second

@item ft/s2
feet per squared second

@item in/s2
inch per squared second

@item rad
radian

@item rpm
revolutions per minute

@end table

@page

And for the English system are:

@sp 2

@table @strong

@item km
kilometer

@item m
meter

@item cm
centimeter

@item mm
millimeter

@item mi
miles

@item in
inch

@item h
hour

@item min
minute

@item km/h
kilometers per hour

@item m/s
meters per second

@item cm/s
centimeters per second

@item mm/s
millimeters per second

@item mph
miles per hour

@item in/s
inch per second

@item kt
knot

@item m/s2
meters per squared second

@item cm/s2
centimeters per squared second

@item mm/s2
millimeters per squared second

@item in/s2
inch per squared second

@item rad
radian

@item rpm
revolutions per minute

@end table

@sp 2

This module have 14 elements, presented below. With a description of each one, its data and its number of equations.


@menu
* Stationary reference system:                          Stationary reference system (ck).  
* Mobile with circular motion:                          Mobile circular (ck).        
* Mobile with polar circular motion:                    Mobile polar (ck).           
* Angular velocity:                                     Angular velocity (ck).       
* Angular acceleration:                                 Angular acceleration (ck).   
* Total acceleration:                                   Total acceleration (ck).     
* Frequency:                                            Frequency (ck).              
* Period:                                               Period (ck).                 
* Number of laps:                                       Number of laps (ck).         
* Center of rotation:                                   Center of rotation (ck).     
* Distance:                                             Distance (ck).               
* Arc length:                                           Arc length (ck).             
* Coordinate:                                           Coordinate (ck).             
* Relative velocity:                                    Relative velocity (ck).      
@end menu

@node Stationary reference system (ck), Mobile circular (ck), Circular kinematics, Circular kinematics
@section Stationary reference system

@cindex Stationary reference system (ck)

@image{s_fijo}

An stationary reference system, with the X axis horizontal and positive to the right, and the Y axis vertical and positive to upwards.

@multitable @columnfractions .15 .6

@item Equations:
@tab None.

@headitem Data:
@tab

@item t:
@tab End time of the problem.

@end multitable

@page


@node Mobile circular (ck), Mobile polar (ck), Stationary reference system (ck), Circular kinematics
@section Mobile with circular motion

@cindex Mobile with circular motion (ck)

@image{m_circular}

Mobile with circular motion (constant radius). The angles are measured from the positive X axis, the positive sense is the opposite of clockwise. Also, a positive value of the tangential velocity, mean that the mobile tour in the opposite sense of clockwise.

@multitable @columnfractions .15 .6

@item Equations:
@tab 4

@headitem Data:
@tab

@item Name:
@tab Name of the mobile.

@item C:
@tab Point that makes the center of rotation. If not specified the origin is the center.

@item r:
@tab Radius of circular motion.

@item aci:
@tab Centripetal acceleration at initial time.

@item at:
@tab Tangential acceleration.

@item angi:
@tab Angle of the initial position.

@item vi:
@tab Initial tangential velocity.

@item ti:
@tab Time at which movements begins.

@item angf:
@tab Angle of the final position.

@item vf:
@tab Final tangential velocity.

@item acf:
@tab Centripetal acceleration at end time.

@end multitable

@page


@node Mobile polar (ck), Angular velocity (ck), Mobile circular (ck), Circular kinematics
@section Mobile with polar circular motion

@cindex Mobile with polar circular motion (ck)

@image{m_polar}

Mobile with circular motion described by polar coordinates. The angles are measured from the X axis, the positive sense is the opposite of clockwise. Also, a positive value to angular velocity, mean that the mobile tour in the opposite sense of clockwise. And a positive value to radial velocity, indicates that the mobile moves away from the center of rotation.

@multitable @columnfractions .15 .6

@item Equations:
@tab 4

@headitem Data:
@tab

@item Name:
@tab Name of the mobile.

@item C:
@tab Point that makes the center of rotation. If not specified the origin is the center.

@item aa:
@tab Angular acceleration.

@item ar:
@tab Radial Acceleration.

@item angi:
@tab Angle of the initial position.

@item ri:
@tab Initial radius of the mobile.

@item vai:
@tab Initial angular velocity.

@item vri:
@tab Initial radial velocity.

@item ti:
@tab Time at which movements begins.

@item angf:
@tab Angle of the final position.

@item rf:
@tab Final radius of the mobile.

@item vaf:
@tab Final angular velocity.

@item vrf:
@tab Final radial velocity.

@end multitable

@page


@node Angular velocity (ck), Angular acceleration (ck), Mobile polar (ck), Circular kinematics
@section Angular velocity

@cindex Angular velocity (ck)

@image{v_angular}

Measure the angular velocity of the mobile indicated. Only applies to elements Mobile with circular motion.

@multitable @columnfractions .15 .6

@item Equations:
@tab 2

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item vangi:
@tab Angular velocity at initial time.

@item vangf:
@tab Angular velocity at end time.

@end multitable


@node Angular acceleration (ck), Total acceleration (ck), Angular velocity (ck), Circular kinematics
@section Angular acceleration

@cindex Angular acceleration (ck)

@image{a_angular}

Measure the angular acceleration of the mobile indicated. Only applies to elements Mobile with circular motion.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item aang:
@tab Angular acceleration.

@end multitable

@page


@node Total acceleration (ck), Frequency (ck), Angular acceleration (ck), Circular kinematics
@section Total acceleration

@cindex Total acceleration (ck)

@image{acel_total}

Measure the total acceleration of the mobile indicated. The angle is measured from the radius vector, the positive sense is the opposite of clockwise.

@multitable @columnfractions .15 .6

@item Equations:
@tab 4

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item atoti:
@tab Magnitude of total acceleration at initial time.

@item aangi:
@tab The angle of the initial total acceleration vector.

@item atotf:
@tab Magnitude of total acceleration at end time.

@item aangf:
@tab The angle of the end total acceleration vector.

@end multitable


@node Frequency (ck), Period (ck), Total acceleration (ck), Circular kinematics
@section Frequency

@cindex Frequency (ck)

@image{frecuencia}

Measure the frequency of the mobile indicated. If the mobile have a tangential o radial acceleration, the measured frequency is the average value.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item f:
@tab Frequency.

@end multitable

@page


@node Period (ck), Number of laps (ck), Frequency (ck), Circular kinematics
@section Period

@cindex Period (ck)

@image{periodo}

Measure the period of the mobile indicated. If the mobile have a tangential o radial acceleration, the measured period is the average value.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item T:
@tab Period.

@end multitable


@node Number of laps (ck), Center of rotation (ck), Period (ck), Circular kinematics
@section Number of laps

@cindex Number of laps (ck)

@image{vueltas}

Measure the number of laps of the mobile indicated.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item n:
@tab Number of laps.

@end multitable

@page


@node Center of rotation (ck), Distance (ck), Number of laps (ck), Circular kinematics
@section Center of rotation

@cindex Center of rotation (ck)

@image{p_estatica}

Especifica un centro de rotaci@'on.

@multitable @columnfractions .15 .6

@item Equations:
@tab None

@headitem Data:
@tab

@item Name:
@tab Name of the point.

@item x:
@tab X coordinate of the point.

@item y:
@tab Y coordinate of the point.

@end multitable


@node Distance (ck), Arc length (ck), Center of rotation (ck), Circular kinematics
@section Distance

@cindex Distance (ck)

@image{distancia}

Measure the distance between two mobiles at final time.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Object 1:
@tab Name of the first object.

@item Object 2:
@tab Name of the second object.

@item d:
@tab Distance between the mobiles.

@end multitable

@page


@node Arc length (ck), Coordinate (ck), Distance (ck), Circular kinematics
@section Arc length

@cindex Arc length (ck)

@image{arco}

Measure the length of the arc described by the mobile indicated. If the mobile have a radial motion, the measured value is approximate.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item s:
@tab Arc length.

@end multitable


@node Coordinate (ck), Relative velocity (ck), Arc length (ck), Circular kinematics
@section Coordinate

@cindex Coordinate (ck)

@image{coordenada}

Get the coordinate of the specified mobile at the end time.

@multitable @columnfractions .15 .6

@item Equations:
@tab 2

@headitem Data:
@tab

@item Object:
@tab Name of the mobile.

@item x:
@tab X coordinate.

@item y:
@tab Y coordinate.

@end multitable

@page


@node Relative velocity (ck),  , Coordinate (ck), Circular kinematics
@section Relative velocity

@cindex Relative velocity (ck)

@image{v_rel}

Measure the relative velocity of mobile Object 1 relative to mobile Object 2. The angle is measured from the positive X axis, the positive sense is the opposite of clockwise. Also, this element can be used to measure the relative velocity (in this case the total velocity) of a Mobile with polar circular motion relative to its center of rotation.

@multitable @columnfractions .15 .6

@item Equations:
@tab 2

@headitem Data:
@tab

@item Object 1:
@tab Name of first mobile.

@item Object 2:
@tab Name of second mobile.

@item v:
@tab Magnitude of the relative velocity.

@item ang:
@tab Angle of the relative velocity vector.

@end multitable
