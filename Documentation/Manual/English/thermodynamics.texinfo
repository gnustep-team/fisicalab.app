@c -*-texinfo-*-
@c This is part of the FisicaLab User Manual.
@c Copyright (C)  2020, 2023 German A. Arias.
@c See the file copying.texinfo for copying conditions.

@node Thermodynamics, , Examples calorimetry, Top
@chapter Thermodynamics

@cindex Thermodynamics

@sp 2

This module have 13 elements, described below. With a description of
each one, its data and its number of equations. In parentheses are
shown the units used be FisicaLab (if no conversion factors are used)
in cases where may be doubt. The elements Adiabatic process,
Isochoric process, Isothermal process, Isobaric process, Heat
machine, Refrigerator and Heat pump support the application of heat
elements (Heat applied and Heat extracted) and work. These elements
are applied to other element placing them in one of the eight cells
around.

@page


@menu
* Applied heat:                                                 Applied heat (therm).          
* Work:                                                         Work (therm).
* Heat extracted:                                               Heat extracted (therm).        
* Adiabatic process:                                            Adiabatic process (therm).         
* Isochoric process:                                            Isochoric process (therm).                 
* Isothermal process:                                           Isothermal process (therm).    
* Isobaric process:                                             Isobaric process (therm).
* Sensible heat:                                                Sensible heat (therm).                   
* Vaporization heat:                                            Vaporization heat (therm).      
* Heat engine:                                                  Heat engine (therm).  
* Refrigerator:                                                 Refrigerator (therm).  
* Heat pump:                                                    Heat pump (therm).
* Gas PV/T:                                                     Gas PV/T (therm).
@end menu

@node Applied heat (therm), Work (therm), Thermodynamics, Thermodynamics
@section Applied heat

@cindex Applied heat (therm)

@image{en_calor}

A certain amount of heat applied.

@multitable @columnfractions .15 .6

@item Equations:
@tab None.

@headitem Data:
@tab

@item Q:
@tab Amount of heat applied (in Joules).

@end multitable


@node Work (therm), Heat extracted (therm), Applied heat (therm), Thermodynamics
@section Work

@cindex Work (therm)

@image{work_machine}

Applied work.

@multitable @columnfractions .15 .6

@item Equations:
@tab None.

@headitem Data:
@tab

@item Work:
@tab Work (in Joules).

@end multitable

@page


@node Heat extracted (therm), Adiabatic process (therm), Work (therm), Thermodynamics
@section Heat extracted

@cindex Heat extracted (therm)

@image{ex_calor}

A certain amount of heat extracted.

@multitable @columnfractions .15 .6

@item Equations:
@tab None.

@headitem Data:
@tab

@item Q:
@tab Amount of heat extracted (in Joules).

@end multitable


@node Adiabatic process (therm), Isochoric process (therm), Heat extracted (therm), Thermodynamics
@section Adiabatic process

@cindex Adiabatic process (therm)

@image{adiabatic_process}

An adiabatic process.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Name:
@tab The name of the element.

@item m:
@tab The mass involved in the process (in kilograms).

@item dU:
@tab Change of the internal energy (in Joules).

@end multitable

@page


@node Isochoric process (therm), Isothermal process (therm), Adiabatic process (therm), Thermodynamics
@section Isochoric process

@cindex Isochoric process (therm)

@image{gas_vc}

An isochoric process.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Name:
@tab The name of the element.

@item m:
@tab The mass involved in the process (in kilograms).

@item dU:
@tab Change of the internal energy (in Joules).

@end multitable


@node Isothermal process (therm), Isobaric process (therm), Isochoric process (therm), Thermodynamics
@section Isothermal process

@cindex Isothermal process (therm)

@image{gas_tc}

An isothermal process.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Name:
@tab The name of the element.

@item m:
@tab The mass involved in the process (in kilograms).

@end multitable

@page


@node Isobaric process (therm), Sensible heat (therm), Isothermal process (therm), Thermodynamics
@section Isobaric process

@cindex Isobaric process (therm)

@image{gas_pc}

An isobaric process.

@multitable @columnfractions .15 .6

@item Equations:
@tab 2

@headitem Data:
@tab

@item Name:
@tab The name of the element.

@item m:
@tab The mass involved in the process (in kilograms).

@item dU:
@tab Change of the internal energy (in Joules).

@item P:
@tab Value of the pressure (in Pascals).

@item Vi:
@tab Initial volume (in m3).

@item Vf:
@tab Final volume (in m3).

@end multitable


@node Sensible heat (therm), Vaporization heat (therm), Isobaric process (therm), Thermodynamics
@section Sensible heat

@cindex Sensible heat (therm)

@image{sensible_heat}

Heat capacity to be used in a process.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Process:
@tab The name of the process.

@item c:
@tab Heat capacity (in J/(kg*K) or J/(kg*C) ).

@item Ti:
@tab Initial temperature (in Kelvins).

@item Tf:
@tab Final temperature (in Kelvins).

@end multitable

@page

@node Vaporization heat (therm), Heat engine (therm), Sensible heat (therm), Thermodynamics
@section Vaporization heat

@cindex Vaporization heat (therm)

@image{cfase_lg}

Vaporization heat to be used in a process.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Process:
@tab The name of the process.

@item cv:
@tab Vaporization heat (in J/kg).

@end multitable

@page


@node Heat engine (therm), Refrigerator (therm), Vaporization heat (therm), Thermodynamics
@section Heat engine

@cindex Heat engine (therm)

@image{heat_engine}

A heat engine.

@multitable @columnfractions .15 .6

@item Equations:
@tab 3

@headitem Data:
@tab

@item TH:
@tab High temperature (in Kelvins).

@item TL:
@tab Low temperature (in Kelvins).

@item e:
@tab Efficiency as percent.

@end multitable

@page

@node Refrigerator (therm), Heat pump (therm), Heat engine (therm), Thermodynamics
@section Refrigerator

@cindex Refrigerator (therm)

@image{refrigerator}

An refriferator.

@multitable @columnfractions .15 .6

@item Equations:
@tab 3

@headitem Data:
@tab

@item TH:
@tab High temperature (in Kelvins).

@item TL:
@tab Low temperature (in Kelvins).

@item cop:
@tab Coefficient of performance.

@end multitable

@page

@node Heat pump (therm), Gas PV/T (therm), Refrigerator (therm), Thermodynamics
@section Heat pump

@cindex Heat pump (therm)

@image{heat_pump}

A heat pump.

@multitable @columnfractions .15 .6

@item Equations:
@tab 3

@headitem Data:
@tab

@item TH:
@tab High temperature (in Kelvins).

@item TL:
@tab Low temperature (in Kelvins).

@item cop:
@tab Coefficient of performance.

@end multitable

@page


@node Gas PV/T (therm), , Heat pump (therm), Thermodynamics
@section Gas PV/T

@cindex Gas PV/T (therm)

@image{gas_pvt}

An ideal gas.

@multitable @columnfractions .15 .6

@item Equations:
@tab 1

@headitem Data:
@tab

@item Pi:
@tab Initial pressure (in Pascals).

@item Vi:
@tab Initial volume.

@item Ti:
@tab Initial temperature (in Kelvins).

@item Pf:
@tab Final pressure (in Pascals).

@item Vf:
@tab Final volume.

@item Tf:
@tab Final temperature (in Kelvins).

@end multitable

@page


