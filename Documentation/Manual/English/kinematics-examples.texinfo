@c -*-texinfo-*-
@c This is part of the FisicaLab User Manual.
@c Copyright (C)  2013 German A. Arias.
@c See the file copying.texinfo for copying conditions.

@node Examples kinematics of particles, Circular kinematics, Kinematics, Top
@chapter Examples kinematics of particles

@cindex Examples kinematics of particles


@menu
* Example 1 (k)::               
* Example 2 (k)::               
* Example 3 (k)::               
* Example 4 (k)::               
* Example 5 (k)::               
* Example 6 (k)::               
* Example 7 (k)::               
@end menu


@node Example 1 (k), Example 2 (k), Examples kinematics of particles, Examples kinematics of particles
@section Example 1

@cindex Example 1 (k)

A car start from the rest with an acceleration of 3 m/s2. Which velocity, in km/h, have after 7 seconds? Which is the travelled distance in meters?

@sp 2

@strong{Solution with FisicaLab}

Select the Kinematic group and, inside this, the Particles module. Erase the content of the chalkboard. And add one element Mobile in X and one element Stationary reference system. As show the image below:

@center @image{k-e1}

We know the end time, 7 seconds. Then in the element Stationary reference system write this.

@sp 1

@table @strong

@item tf
7

@end table

@sp 1

To the element  Mobile in X, we assume that the car begins its movement from the X coordinate 0 and at the time 0. Also, we assume that its movement is in the direction of the positive X axis. But, as we want the final velocity in km/h, we need write:

@sp 1

@example

vf @@ km/h

@end example

@sp 1

To the final velocity. As show below:

@sp 1

@table @strong

@item Name
Car

@item ax
3

@item xi
0

@item vxi
0

@item ti
0

@item xf
d

@item vxf
vf @@ km/h

@end table

@sp 1

Where @emph{d} is the final X coordinate, an unknown data. To this example, the name of the mobile @emph{Car} is irrelevant. Now click in the icon Solve to get the answer.

@example

d = 73.500 m ;  vf = 75.600 km/h ;
Status = success.

@end example

@page


@node Example 2 (k), Example 3 (k), Example 1 (k), Examples kinematics of particles
@section Example 2

@cindex Example 2 (k)

A car start from a place B with a constant velocity of 75 km/h at the east. 8 minutes after, other car with a constant velocity of 88 km/h start from a place A to the east. The place A is 5 km to the west of B. How minutes needs the last car to reach the first car? In that moment, which is the distance between the place A and the cars?

@sp 2

@strong{Solution with FisicaLab}

Select the Kinematic group and, inside this, the Particles module. Erase the content of the chalkboard. And add two elements Mobile in X with constant velocity and one element Stationary reference system. As show the image below:

@center @image{k-e2}

We assume that the Mobile in X at the left, corresponds to the car that start from A. Now, the end time, is an unknown data, then in the element Stationary reference system write:

@sp 1

@table @strong

@item tf
t @@ min

@end table

@sp 1

Because we want the end time in minutes. Now, we assume that the car that start from A is in the origin of the stationary reference system, and start at 8 minutes. We call this car @emph{Car A}:

@sp 1

@table @strong

@item Name
Car A

@item xi
0

@item xf
d @@ km

@item ti
8 @@ min

@item vx
88 @@ km/h

@end table

@sp 1

To the car that start from B. Its initial X coordinate is 5 km (because the other car is in the origin), and the start time is 0. We call this car @emph{Car B}:

@sp 1

@table @strong

@item Name
Car B

@item xi
5 @@ km

@item xf
d @@ km

@item ti
0

@item vx
75 @@ km/h

@end table

@sp 1

The final X coordinate of both cars is the same, because we want know the moment when the last car reach the first car. Now click in the icon Solve to get the answer.

@example

d = 101.538 k m ;  t = 77.231 min ;
Status = success.

@end example

@page


@node Example 3 (k), Example 4 (k), Example 2 (k), Examples kinematics of particles
@section Example 3

@cindex Example 3 (k)

Two cars start from the same place. One with a constant velocity of 80 km/h at 50 degrees from the east, in the direction north-east. The other with a constant velocity of 95 km/h to the east. In what moment the distance between these cars is 50 km? Give your answer in minutes.

@sp 2

@strong{Solution with FisicaLab}

Select the Kinematic group and, inside this, the Particles module. Erase the content of the chalkboard. And add one element Mobile in X whit constant velocity, one element Mobile radial, one element Distance and one element Stationary reference system. As show the image below:

@center @image{k-e3}

The end time, is an unknown data, then in the element Stationary reference system write:

@sp 1

@table @strong

@item tf
t @@ min

@end table

@sp 1

Because we want the time in minutes. Now, we assume that both cars start from the origin of the stationary reference system, and at the time 0. Then, to the element Mobile radial write, we call this car @emph{Car 1}:

@sp 1

@table @strong

@item Name
Car 1

@item a
0

@item angf
50

@item xi
0

@item yi
0

@item vi
88 @@ km/h

@item ti
0

@item xf
xf1

@item yf
yf1

@item vf
vf1

@end table

@sp 1

We write the final velocity as an unknown data. Although, the acceleration is 0. We do this to satisfied @emph{numbers of equations = numbers of unknown data}. Now, to the Mobile in X write, we call this @emph{Car 2}:

@sp 1

@table @strong

@item Name
Car 2

@item xi
0

@item xf
xf2

@item ti
0

@item vx
95 @@ km/h

@end table

@sp 1

To the element Distance:

@sp 1

@table @strong

@item Object 1
Car 1

@item Object 2
Car 2

@item d
50 @@ km

@end table

@sp 1

Where Object 1 and Object 2 are, respectively, the @emph{Car 1} and @emph{Car 2}. And @emph{d} is the distance between both cars, in this case 50 kilometers.

Now click in the icon Solve to get the answer.

@example

xf1 = 36447.231 m ;  yf1 = 43436.119 m ;
vf1 = 24.444 m/s ;  t = 38.660 min ;
xf2 = 61212.198 m ;
Status = success.

@end example

The desired data is t = 38.660 min.

@page


@node Example 4 (k), Example 5 (k), Example 3 (k), Examples kinematics of particles
@section Example 4

@cindex Example 4 (k)

A cannon shot a projectile at 35 degrees from the horizontal, with a velocity of 17 m/s. We want know the maximum altitude, and the range.

@sp 2

@strong{Solution with FisicaLab}

Select the Kinematic group and, inside this, the Particles module. Erase the content of the chalkboard. And add one element Cannon and one element Stationary reference system. As show the image below:

@center @image{k-e4}

The end time, is an unknown data, then in the element Stationary reference system write:

@sp 1

@table @strong

@item tf
t

@end table

@sp 1

The data of the maximum altitude and range correspond at different times. Then, we need first set the problem to determine the maximum altitude, and after set the problem to determine the range.

@sp 2

@strong{Maximum altitude}

To this case, to the element Cannon write:

@sp 1

@table @strong

@item Name
Projectile

@item ax
0

@item ay
-9.81

@item xi
0

@item yi
0

@item vi
17

@item angi
35

@item ti
0

@item xf
xf

@item yf
yf

@item vf
vf

@item angf
0

@end table

@sp 1

The acceleration in X axis is 0, but in Y is -9.81 (the acceleration of gravity). We assume that the cannon shot at time 0. And that this is located at the origin of the stationary system. Also, we know that, at the maximum altitude, the angle of the velocity vector is 0. 

Now click in the icon Solve to get the answer.

@example

xf = 13.842 m ;  yf = 4.846 m ;  vf = 13.926 m/s ;
t = 0.994 s ;
Status = success.

@end example

The desired data is yf = 4.846 m.

@sp 2

@strong{Range}

There are many ways to calculate the range. Here, we write to the end Y coordinate 0, and to the final velocity and the end angle write unknown data. This is necessary to satisfied numbers of equations = numbers of unknown data. Then, write:

@sp 1

@table @strong

@item Name
Projectile

@item ax
0

@item ay
-9.81

@item xi
0

@item yi
0

@item vi
17

@item angi
35

@item ti
0

@item xf
xf

@item yf
0

@item vf
vf

@item angf
angf

@end table

@sp 1

Now click in the icon Solve to get the answer:

@example

xf = 27.683 m ;  vf = 17.000 m/s ;  angf = -35.000 degrees ;
t = 1.988 s ;
Status = success.

@end example

@strong{NOTE:} @emph{If you want the altitude in other position, keep in mind that, in general, are two solutions. The first when the projectile is ascending and the second when the projectile is descending. If you want the second position, you need set the initial position (the start position) after the first position.}

@page


@node Example 5 (k), Example 6 (k), Example 4 (k), Examples kinematics of particles
@section Example 5

@cindex Example 5 (k)

A swimmer want to cross a river with a width of 25 meters. Its current have a velocity of 7 m/s. The swimmer take a perpendicular direction to the rive. If the swimmer have a velocity of 7 m/s, how minutes needs to cross the river?, what distance was dragged the swimmer?

@sp 2

@strong{Solution with FisicaLab}

The velocity of the swimmer is in reference to the river, that is a mobile reference system in reference to the share. With this in mind, erase the content of the chalkboard. And add one element Mobile reference system in X, one element Mobile in Y with constant velocity, and one element Stationary reference system. As show the image below:

@center @image{k-e5}

We assume that the river's movement is in the direction of the positive X axis. That the swimmer take the direction of the positive Y axis. And that both, the reference system and the swimmer, start from the origin of the stationary reference system. The end time is an unknown data:

@sp 1

@table @strong

@item tf
t

@end table

@sp 1

To the Mobile in Y with constant velocity, we call this @emph{Swimmer}, write:

@sp 1

@table @strong

@item Name
Swimmer

@item yi
0

@item yf
25

@item ti
0

@item vy
2

@end table

@sp 1

To the Mobile reference system in X, write:

@sp 1

@table @strong

@item Name
Mobile system

@item Object
Swimmer

@item xsi
0

@item vsx
7

@item xof
xf

@item vxof
vf

@end table

@sp 1

We write the final velocity to the swimmer in reference to the stationary system, vxof, as an unknown data, to satisfied @emph{numbers of equations = numbers of unknown data}. Of course, this velocity is 7 m/s, because the swimmer don't have a velocity in X axis. Now click in the icon Solve to get the answer.

@example

t = 12.500 s ;  xf = 87.500 m ;  vf = 7.000 m/s ;
Status = success.

@end example

@page


@node Example 6 (k), Example 7 (k), Example 5 (k), Examples kinematics of particles
@section Example 6

@cindex Example 6 (k)

Two cars, A and B, start from the same place. A take the direction of the north with a constant velocity of 80 km/h, and B the direction of the east, with an acceleration of 4 m/s2 (start from the rest). After 13 seconds, which is the relative velocity of the car A in reference to B?

@sp 2

@strong{Solution with FisicaLab}

Erase the content of the chalkboard. And add one element Mobile in X, one element Mobile in Y with constant velocity, one element Relative velocity, and one element Stationary reference system. As show the image below:

@center @image{k-e6}

We assume that the car A take the direction of the positive Y axis, and car B the direction of the positive X axis. And that both cars start from the origin. Now, as we know the end time, in the element Stationary reference system, write:

@sp 1

@table @strong

@item tf
13

@end table

@sp 1

To the element Mobile in Y with constant velocity, we call this car @emph{Car A}, write:

@sp 1

@table @strong

@item Name
Car A

@item yi
0

@item yf
yf

@item ti
0

@item vy
80 @@ km/h

@end table

@sp 1

To the element Mobile in X, we call this car @emph{Car B}:

@sp 1

@table @strong

@item Name
Car B

@item ax
4

@item xi
0

@item vxi
0

@item ti
0

@item xf
xf

@item vxf
vxf

@end table

@sp 1

And to the element Relative velocity:

@sp 1

@table @strong

@item Object 1
Car A

@item Object 2
Car B

@item v
v

@item ang
ang

@end table

@sp 1

Where the magnitude and the angle of the relative velocity are unknown data. Now click in the icon Solve to get the answer:

@example

v = 56.549 m/s ;  ang = 156.861 degrees ;  xf = 338.000 m ;
vxf = 52.000 m/s ;  yf = 288.889 m ;
Status = success.

@end example

The desired data are v = 56.549 m/s and ang = 156.861 degrees.

@page


@node Example 7 (k),  , Example 6 (k), Examples kinematics of particles
@section Example 7

@cindex Example 7 (k)

A car A travel with a velocity of 110 km/h, in some moment pass together a B car, that is in rest. 4 seconds after, the car B start with an acceleration of 2.6 m/s2 in chase to the car A. How seconds need to be 5 meters behind the car A?

@sp 2

@strong{Solution with FisicaLab}

Erase the content of the chalkboard. And add one element Mobile in X, one element Mobile in X with constant velocity, one element Distance XY, and one element Stationary reference system. As show the image below:

@center @image{k-e7}

The end time is an unknown data:

@sp 1

@table @strong

@item tf
t

@end table

@sp 1

We assume that the car A is moving in the direction of the positive X axis. And that both cars start from the origin. The car A is the element Mobile in X with constant velocity. To this, write:

@sp 1

@table @strong

@item Name
Car A

@item xi
0

@item xf
xfA

@item ti
0

@item vx
110 @@ km/h

@end table

@sp 1

To the element Mobile in X, the car B, write (remember that this car begins its movement after 4 seconds):

@sp 1

@table @strong

@item Name
Car B

@item ax
2.6

@item xi
0

@item vxi
0

@item ti
4

@item xf
xfB

@item vxf
vfB

@end table

@sp 1

To the element Distance XY:

@sp 1

@table @strong

@item x1 (y1)
xfA

@item x2 (y2)
xfB

@item x1 - x2
@itemx (y1 - y2)
5

@end table

@sp 1

Now click in the icon Solve to get the answer.

@example

t = 30.861 s ;  xfA = 942.981 m ;  xfB 937.981 ;
vfB = 69.839 m/s ;
Status = success.

@end example
