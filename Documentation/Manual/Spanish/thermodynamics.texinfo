@c -*-texinfo-*-
@c This is part of the FisicaLab User Manual.
@c Copyright (C)  2022, 2023 German A. Arias.
@c See the file copying.texinfo for copying conditions.

@node Termodinamica, Ejemplos termodinamica, Ejemplos calorimetria, Top
@chapter Termodin@'amica

@cindex Termodin@'amica

@sp 2

Este m@'odulo consta de 13 elementos. Estos se presentan abajo, con
una descripci@'on de cada uno, sus datos y el n@'umero de ecuaciones
que utilizan. Entre par@'entesis se especifican las dimensionales
que F@'{@dotless{i}}sicaLab aplica (si no se utilizan factores de
conversi@'on) para aquellos datos en que pueda haber duda. Los
elementos Proceso adiab@'atico, Proceso isoc@'orico, Proceso
isot@'ermico, Proceso isob@'arico, M@'aquina t@'ermica, Refrigerador
y Bomba de calor soportan la aplicaci@'on de elementos de calor (
Calor aplicado y calor extra@'{@dotless{i}}do) y Trabajo. Estos
elementos se aplican a un elemento dado coloc@'andolo en alguna de
las ocho casillas alrededor de este.

@page


@menu
* Calor aplicado:                                               Calor aplicado (therm).          
* Trabajo:                                                      Trabajo (therm).
* Calor extra@'{@dotless{i}}do:                                 Calor extraido (therm).        
* Proceso adiab@'atico:                                         Proceso adiabatico (therm).         
* Proceso isoc@'orico:                                          Proceso isicorico (therm).                 
* Proceso isot@'ermico:                                         Proceso isotermico (therm).    
* Proceso isob@'arico:                                          Proceso isobarico (therm).
* Calor sensible:                                               Calor sensible (therm).                   
* Calor de vaporizaci@'on:                                      Calor de vaporizacion (therm).      
* M@'aquina t@'ermica:                                          Maquina termica (therm).  
* Refrigerador:                                                 Refrigerador (therm).  
* Bomba de calor:                                               Bomba de calor (therm).  
* Gas PV/T:                                                     Gas PV/T (therm).
@end menu

@node Calor aplicado (therm), Trabajo (therm), Termodinamica, Termodinamica
@section Calor aplicado

@cindex Calor aplicado (therm)

@image{en_calor}

Una cierta cantidad de calor aplicado.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab Ninguna.

@headitem Datos:
@tab

@item Q:
@tab Cantidad de calor aplicado (en Joules).

@end multitable


@node Trabajo (therm), Calor extraido (therm), Calor aplicado (therm), Termodinamica
@section Trabajo

@cindex Trabajo (therm)

@image{work_machine}

Trabajo aplicado.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab Ninguna.

@headitem Datos:
@tab

@item Trabajo:
@tab Trabajo (en Joules).

@end multitable

@page


@node Calor extraido (therm), Proceso adiabatico (therm), Trabajo (therm), Termodinamica
@section Calor extra@'{@dotless{i}}do

@cindex Calor extra@'{@dotless{i}}do (therm)

@image{ex_calor}

Una cierta cantidad de calor extra@'{@dotless{i}}do.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab Ninguna.

@headitem Datos:
@tab

@item Q:
@tab Cantidad de calor extra@'{@dotless{i}}do (en Joules).

@end multitable


@node Proceso adiabatico (therm), Proceso isocorico (therm), Calor extraido (therm), Termodinamica
@section Proceso adiab@'atico

@cindex Proceso adiab@'atico (therm)

@image{adiabatic_process}

Un proceso adiab@'atico.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 1

@headitem Datos:
@tab

@item Nombre:
@tab Nombre del elemento.

@item m:
@tab La masa involucrada en el proceso (en kilogramos).

@item dU:
@tab Cambio de la energ@'{@dotless{i}}a interna (en Joules).

@end multitable

@page


@node Proceso isocorico (therm), Proceso isotermico (therm), Proceso adiabatico (therm), Termodinamica
@section PRoceso isic@'orico

@cindex Proceso isoc@'orico (therm)

@image{gas_vc}

Un proceso isoc@'orico.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 1

@headitem Datos:
@tab

@item Nombre:
@tab El nombre del elemento.

@item m:
@tab La masa involucrada en el proceso (en kilogramos).

@item dU:
@tab Cambio de la energ@'{@dotless{i}}a interna (en Joules).

@end multitable


@node Proceso isotermico (therm), Proceso isobarico (therm), Proceso isocorico (therm), Termodinamica
@section Proceso isot@'ermico

@cindex Proceso isot@'ermico (therm)

@image{gas_tc}

Un proceso isot@'ermico.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 1

@headitem Datos:
@tab

@item Nombre:
@tab El nombre del elemento.

@item m:
@tab La masa involucrada en el proceso (en kilogramos).

@end multitable

@page


@node Proceso isobarico (therm), Calor sensible (therm), Proceso isotermico (therm), Termodinamica
@section Proceso isob@'arico

@cindex Proceso isob@'arico (therm)

@image{gas_pc}

Un proceso isob@'arico.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 2

@headitem Datos:
@tab

@item Nombre:
@tab El nombre del elemento.

@item m:
@tab La masa involucrada en el proceso (en kilogramos).

@item dU:
@tab Cambio de la energ@'{@dotless{i}}a interna (en Joules).

@item P:
@tab Valor de la presi@'on (en Pascales).

@item Vi:
@tab Volumen inicial (en m3).

@item Vf:
@tab Volumen final (en m3).

@end multitable


@node Calor sensible (therm), Calor de vaporizacion (therm), Proceso isobarico (therm), Termodinamica
@section Calor sensible

@cindex Calor sensible (therm)

@image{sensible_heat}

Calor espec@'{@dotless{i}}fico para ser usado en un proceso.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 1

@headitem Datos:
@tab

@item Proceso:
@tab El nombre del proceso.

@item c:
@tab Calor espec@'{@dotless{i}}fico (en J/(kg*K) o J/(kg*C) ).

@item Ti:
@tab Temperatura inicial (en Kelvins).

@item Tf:
@tab Temperatura final (en Kelvins).

@end multitable

@page

@node Calor de vaporizacion (therm), Maquina termica (therm), Calor sensible (therm), Termodinamica
@section Calor de vaporizaci@'on

@cindex Calor de vaporizaci@'on (therm)

@image{cfase_lg}

Calor de vaporizaci@'on para ser usado en un proceso.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 1

@headitem Datos:
@tab

@item Proceso:
@tab Nombre del proceso.

@item cv:
@tab Calor de vaporizaci@'on (en J/kg).

@end multitable

@page


@node Maquina termica (therm), Refrigerador (therm), Calor de vaporizacion (therm), Termodinamica
@section M@'aquina t@'ermica

@cindex M@'aquina t@'ermica (therm)

@image{heat_engine}

Una m@'aquina t@'ermica.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 3

@headitem Datos:
@tab

@item TH:
@tab Temperatura alta (en Kelvins).

@item TL:
@tab Temperatura baja (en Kelvins).

@item e:
@tab Eficiencia.

@end multitable

@page

@node Refrigerador (therm), Bomba de calor (therm), Maquina termica (therm), Termodinamica
@section Refrigerador

@cindex Refrigerador (therm)

@image{refrigerator}

Un refriferador.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 3

@headitem Datos:
@tab

@item TH:
@tab Temperatura alta (en Kelvins).

@item TL:
@tab Temperatura baja (en Kelvins).

@item cop:
@tab Coeficiente de rendimiento.

@end multitable

@page

@node Bomba de calor (therm), Gas PV/T (therm), Refrigerador (therm), Termodinamica
@section Bomba de calor

@cindex Bomba de calor (therm)

@image{heat_pump}

Una bomba de calor.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 3

@headitem Datos:
@tab

@item TH:
@tab Temperatura alta (en Kelvins).

@item TL:
@tab Temperatura baja (en Kelvins).

@item cop:
@tab Coeficiente de rendimiento.

@end multitable

@page


@node Gas PV/T (therm), , Bomba de calor (therm), Termodinamica
@section Gas PV/T

@cindex Gas PV/T (therm)

@image{gas_pvt}

Un gas ideal.

@multitable @columnfractions .15 .6

@item Ecuaciones:
@tab 1

@headitem Datos:
@tab

@item Pi:
@tab Presi@'on inicial (en Pascales).

@item Vi:
@tab Volumen inicial.

@item Ti:
@tab Temperatura inicial (en Kelvins).

@item Pf:
@tab Presi@'on final (en Pascales).

@item Vf:
@tab Volumen final.

@item Tf:
@tab Temperatura final (en Kelvins).

@end multitable

@page


