@c -*-texinfo-*-
@c This is part of the FisicaLab User Manual.
@c Copyright (C)  2013 German A. Arias.
@c See the file copying.texinfo for copying conditions.

@node Ejemplos cinematica circular, Estatica, Cinematica circular, Top
@chapter Ejemplos de cinem@'atica circular de part@'{@dotless{i}}culas

@cindex Ejemplos de cinem@'atica circular de part@'{@dotless{i}}culas


@menu
* Ejemplo 1 (cc)::              
* Ejemplo 2 (cc)::              
* Ejemplo 3 (cc)::              
* Ejemplo 4 (cc)::              
* Ejemplo 5 (cc)::              
@end menu


@node Ejemplo 1 (cc), Ejemplo 2 (cc), Ejemplos cinematica circular, Ejemplos cinematica circular
@section Ejemplo 1

@cindex Ejemplo 1 (cc)

Un m@'ovil con movimiento circular uniforme recorre una circunferencia de radio 3 m con una velocidad tangencial constante de 27 m/s, @questiondown{}Cual es la aceleraci@'on centr@'{@dotless{i}}peta del m@'ovil? @questiondown{}Que distancia en metros ha recorrido despu@'es de 6.3 segundos?

@sp 2

@strong{Soluci@'on con F@'{@dotless{i}}sicaLab}

Seleccionamos el grupo Cinem@'atica y, dentro de este, el m@'odulo Circular. Si la Pizarra contiene elementos, los borramos dando un clic en el icono Borrar todo. Seleccionamos entonces el sistema de unidades SI (el sistema de unidades por defecto). Y ahora agregamos a la Pizarra un elemento M@'ovil con movimiento circular, un elemento Longitud de arco y un Sistema de referencia fijo, como muestra la siguiente imagen:

@center @image{kc-e1, 4cm}

Para el elemento Sistema de referencia fijo tenemos:

@sp 1

@table @strong

@item tf
6.3

@end table

@sp 1

Ahora al elemento M@'ovil con movimiento circular lo llamaremos @emph{Movil}. Y suponemos que su posici@'on inicial es la que corresponde al @'angulo 0. Asimismo, dejamos el centro como 0 (para este problema no es necesario establecer un centro de rotaci@'on). Los datos de velocidad final y aceleraci@'on centr@'{@dotless{i}}peta los ponemos como inc@'ognitas, puesto que el elemento tiene un dato de aceleraci@'on tangencial y se asume que la velocidad final puede ser diferente a la inicial:

@sp 1

@table @strong

@item Nombre
Movil

@item C
0

@item r
3

@item aci
aci

@item at
0

@item angi
0

@item vi
27

@item ti
0

@item angf
angf

@item vf
vf

@item acf
acf

@end table

@sp 1

Y para el elemento Longitud de arco el objeto a medir sera nuestro @emph{Movil}, y la longitud del arco es por supuesto una inc@'ognita:

@sp 1

@table @strong

@item Objeto
Movil

@item s
s

@end table

@sp 1

Ingresados los datos, damos un clic en el icono de Resolver para obtener las respuestas:

@example

aci = 243.000 m/s2 ;  angf = 8.671 grados ;
vf = 27.000 m/s ;  acf = 243.000 m/s2 ;
s = 170.100 m ;
Estado = success.

@end example

@page


@node Ejemplo 2 (cc), Ejemplo 3 (cc), Ejemplo 1 (cc), Ejemplos cinematica circular
@section Ejemplo 2

@cindex Ejemplo 2 (cc)

Un m@'ovil parte del reposo describiendo un movimiento circular con radio de 4.7 m. Si la aceleraci@'on tangencial es de 1.6 m/s2, @questiondown{}Cuantos segundos necesita para recorrer una distancia de 256 m? @questiondown{}A cuantas vueltas equivale esa distancia? @questiondown{}Cual es su aceleraci@'on total en ese momento (despu@'es de recorrer los 256 m)?

@sp 2

@strong{Soluci@'on con F@'{@dotless{i}}sicaLab}

Seleccionamos el grupo Cinem@'atica y, dentro de este, el m@'odulo Circular. Si la Pizarra contiene elementos, los borramos dando un clic en el icono Borrar todo. Seleccionamos entonces el sistema de unidades SI. Y ahora agregamos a la Pizarra un elemento M@'ovil con movimiento circular, un elemento Longitud de arco, un elemento N@'umero de vueltas, un elemento Aceleraci@'on total y un Sistema de referencia fijo, como muestra la siguiente imagen:

@center @image{kc-e2, 4cm}

Como el tiempo es una inc@'ognita, tenemos para el elemento Sistema de referencia fijo:

@sp 1

@table @strong

@item tf
t

@end table

@sp 1

Ahora para el elemento M@'ovil con movimiento circular, al que llamaremos @emph{Movil}, asumimos que su posici@'on inicial corresponde al @'angulo 0. Tampoco establecemos un centro de rotaci@'on (no es necesario para este problema):

@sp 1

@table @strong

@item Nombre
Movil

@item C
0

@item r
4.7

@item aci
aci

@item at
1.6

@item angi
0

@item vi
0

@item ti
0

@item angf
angf

@item vf
vf

@item acf
acf

@end table

@sp 1

Para el elemento Longitud de arco:

@sp 1

@table @strong

@item Objeto
Movil

@item s
256

@end table

@sp 1

Para el elemento N@'umero de vueltas tenemos:

@sp 1

@table @strong

@item Objeto
Movil

@item n
n

@end table

@sp 1

Y para el elemento Aceleraci@'on total:

@sp 1

@table @strong

@item Objeto
Movil

@item atoti
atoti

@item angi
aangi

@item atotf
atotf

@item angf
aangf

@end table

@sp 1

Ingresados los datos, damos un clic en el icono de Resolver para obtener las respuestas:

@example

aci = -0.000 m/s2 ;  angf = 240.791 grados ;
vf = 28.622 m/s ;  acf = 174.298 m/s2 ;
t = 17.889 s ;  n = 8.669 rev ;
atoti = 1.600 m/s2 ;  aangi = 90.000 grados ;
atotf = 174.305 m/s2 ;  aangf = 179.474 grados ;
Estado = success.

@end example

@page


@node Ejemplo 3 (cc), Ejemplo 4 (cc), Ejemplo 2 (cc), Ejemplos cinematica circular
@section Ejemplo 3

@cindex Ejemplo 3 (cc)

Un m@'ovil con movimiento circular uniforme describe una circunferencia de radio 7.5 m. Si la velocidad angular del m@'ovil es de 1.3 rad/s, @questiondown{}Cuantos segundos necesita para describir 9 giros? @questiondown{}Cual es su aceleraci@'on centr@'{@dotless{i}}peta? @questiondown{}Cual es su periodo?

@sp 2    

@strong{Soluci@'on con F@'{@dotless{i}}sicaLab}

Seleccionamos el grupo Cinem@'atica y, dentro de este, el m@'odulo Circular. Si la Pizarra contiene elementos, los borramos dando un clic en el icono Borrar todo. Seleccionamos entonces el sistema de unidades SI. Y ahora agregamos a la Pizarra un elemento M@'ovil con movimiento circular, un elemento Velocidad angular, un elemento N@'umero de vueltas, un elemento Periodo y un Sistema de referencia fijo, como muestra la siguiente imagen:

@center @image{kc-e3, 4cm}

Como el tiempo es una inc@'ognita, tenemos para el elemento Sistema de referencia fijo:

@sp 1

@table @strong

@item tf
t

@end table

@sp 1

Para el elemento M@'ovil con movimiento circular, que llamaremos @emph{Movil}, tanto la velocidad inicial como la final son inc@'ognitas (las respectivas aceleraciones centr@'{@dotless{i}}petas tambi@'en son inc@'ognitas):

@sp 1

@table @strong

@item Nombre
Movil

@item C
0

@item r
7.5

@item aci
0

@item at
0

@item angi
0

@item vi
vi

@item ti
0

@item angf
angf

@item vf
vf

@item acf
acf

@end table

@sp 1

En el elemento Velocidad angular la velocidad angular 1.3 rad/s ser@'a la velocidad angular constante del m@'ovil, puesto que este no tiene una aceleraci@'on tangencial. Sin embargo, para cumplir con la igualdad del n@'umero de inc@'ognitas y el n@'umero de ecuaciones, colocamos este dato como velocidad angular inicial y dejamos el dato final como inc@'ognita, aunque tenga el mismo valor:

@sp 1

@table @strong

@item Objeto
Movil

@item vangi
1.3

@item vangf
vangf

@end table

@sp 1

Para el elemento N@'umero de vueltas:

@sp 1

@table @strong

@item Objeto
Movil

@item n
9

@end table

@sp 1

Y para el elemento Periodo:

@sp 1

@table @strong

@item Objeto
Movil

@item T
T

@end table

@sp 1

Ingresados los datos, damos un clic en el icono de Resolver para obtener las respuestas:

@example

aci = 12.675 m/s2 ;  vi = 9.750 m/s ;
angf = 360.000 grados ;  vf = 9.750 m/s ;
acf = 12.675 m/s2 ;  t = 43.499 s ;
vangf = 1.300 rad/s ;  T = 4.833 1/hz ;
Estado = success.

@end example

@page


@node Ejemplo 4 (cc), Ejemplo 5 (cc), Ejemplo 3 (cc), Ejemplos cinematica circular
@section Ejemplo 4

@cindex Ejemplo 4 (cc)

Un m@'ovil parte del reposo desde el origen de un sistema coordenado, con una velocidad angular constante de 0.4 rad/s, una velocidad radial inicial de 0.2 m/s (en la direcci@'on del eje X positivo) y con una aceleraci@'on radial de 0.12 m/s2. @questiondown{}Cual es la coordenada de su posici@'on luego de 5 segundos? @questiondown{}Cual es su velocidad radial en ese momento? @questiondown{}A que distancia se encuentra del origen? @questiondown{}Cual es la frecuencia promedio de su movimiento circular?

@sp 2

@strong{Soluci@'on con F@'{@dotless{i}}sicaLab}

Seleccionamos el grupo Cinem@'atica y, dentro de este, el m@'odulo Circular. Si la Pizarra contiene elementos, los borramos dando un clic en el icono Borrar todo. Seleccionamos entonces el sistema de unidades SI. Y ahora agregamos a la Pizarra un elemento M@'ovil con movimiento circular polar, un elemento Coordenada, un elemento Frecuencia y un Sistema de referencia fijo, como muestra la siguiente imagen:

@center @image{kc-e4, 4cm}

Como el tiempo es un dato conocido, tenemos para el elemento Sistema de referencia fijo:

@sp 1

@table @strong

@item tf
5

@end table

@sp 1

Ahora para el elemento M@'ovil con movimiento circular polar. Le colocamos el nombre @emph{Movil}. Y su posici@'on inicial corresponde al @'angulo 0 (ya que la velocidad radial inicial tiene la direcci@'on del eje X positivo). El centro lo dejamos establecido a 0, puesto que el m@'ovil gira alrededor del origen. El radio inicial lo colocamos a 0 (puesto que parte del origen), la aceleraci@'on angular es tambi@'en 0, y la velocidad angular final la colocamos como inc@'ognita, ya que al haber un dato de aceleraci@'on angular se asume que la velocidad angular final puede ser diferente a la inicial. Y la velocidad radial final tambi@'en queda establecida como inc@'ognita:

@sp 1

@table @strong

@item Nombre
Movil

@item C
0

@item aa
0

@item ar
0.12

@item angi
0

@item ri
0

@item vai
0.4

@item vri
0.2

@item ti
0

@item angf
angf

@item rf
rf

@item vaf
vaf

@end table

@sp 1

Para el elemento Coordenada colocamos @emph{Movil} como objeto a medir, y las coordenadas X y Y quedan como inc@'ognitas:

@sp 1

@table @strong

@item Objeto
Movil

@item x
x

@item y
y

@end table

@sp 1

Y en el elemento Frecuencia el dato de frecuencia queda como inc@'ognita:

@sp 1

@table @strong

@item Objeto
Movil

@item f
f

@end table

@sp 1

Ingresados los datos, damos un clic en el icono de Resolver para obtener las respuestas:

@example

x = -1.040 m ;  y = 2.273 m ;  f = 0.064 hz ;
angf = 114.592 grados ;  rf = 2.500 m ;
vaf = 0.400 rad/s ;  vrf = 0.800 m/s ;
Estado = success.

@end example

@page


@node Ejemplo 5 (cc),  , Ejemplo 4 (cc), Ejemplos cinematica circular
@section Ejemplo 5

@cindex Ejemplo 5 (cc)

Un m@'ovil A con movimiento circular uniforme, gira alrededor del origen describiendo una circunferencia de 83 cm con una velocidad tangencial de 0.7 m/s. En el momento en que dicho m@'ovil esta a 59 grados sobre la horizontal, un m@'ovil B parte del punto (10, 17)cm con una velocidad radial constante de 8 cm/s (en la direcci@'on del eje X positivo), y una velocidad angular constante de 0.6 rad/s. 12 segundos despu@'es de la partida del m@'ovil B, @questiondown{}Que distancia separa a ambos m@'oviles? @questiondown{}Cual es la velocidad relativa del m@'ovil B con respecto al m@'ovil A? @questiondown{}Cuantas vueltas a descrito cada uno de los m@'oviles?

@sp 2

@strong{Soluci@'on con F@'{@dotless{i}}sicaLab}

Seleccionamos el grupo Cinem@'atica y, dentro de este, el m@'odulo Circular. Si la Pizarra contiene elementos, los borramos dando un clic en el icono Borrar todo. Seleccionamos entonces el sistema de unidades SI. Y ahora agregamos a la Pizarra un elemento M@'ovil con movimiento circular, un elemento M@'ovil con movimiento circular polar, un elemento Centro de rotaci@'on, un elemento Distancia, un elemento Velocidad relativa, dos elementos N@'umero de vueltas y un Sistema de referencia fijo, como muestra la siguiente imagen:

@center @image{kc-e5, 4cm}

Para el elemento Sistema de referencia fijo tenemos:

@sp 1

@table @strong

@item tf
12

@end table

@sp 1

Para el elemento M@'ovil con movimiento circular (el m@'ovil A que gira alrededor del origen), tenemos:

@sp 1

@table @strong

@item Nombre
A

@item C
0

@item r
83 @@ cm

@item aci
aciA

@item at
0

@item angi
59

@item vi
0.7

@item ti
0

@item angf
angfA

@item vf
vfA

@item acf
acfA

@end table

@sp 1

El elemento Centro de rotaci@'on, que sera el centro del m@'ovil B, queda como:

@sp 1

@table @strong

@item Nombre
Centro

@item x
10 @@ cm

@item y
17 @@ cm

@end table

@sp 1

Para el elemento M@'ovil con movimiento circular polar tenemos:

@sp 1

@table @strong

@item Nombre
B

@item C
Centro

@item aa
0

@item ar
0

@item angi
0

@item ri
0

@item vai
0.6

@item vri
8 @@ cm/s

@item ti
0

@item angf
angfB

@item rf
rfB

@item vaf
vafB

@item vrf
vrfB

@end table

@sp 1

El elemento Distancia:

@sp 1

@table @strong

@item Objeto 1
A

@item Objeto 2
B

@item d
d

@end table

@sp 1

Para el elemento Velocidad relativa tenemos:

@sp 1

@table @strong

@item Objeto 1
B

@item Objeto 2
A

@item v
vrBA

@item ang
angBA

@end table

@sp 1

Y los elementos N@'umero de vueltas, uno para cada m@'ovil:

@sp 1

@table @strong

@item Objeto
A

@item n
nA

@end table

@sp 2

@table @strong

@item Objeto
B

@item n
nB

@end table

@sp 1

Ingresados los datos, damos un clic en el icono de Resolver para obtener las respuestas:

@example

aciA = 0.590 m/s2 ;  angfA = 278.861 grados ;
vfA = 0.700 m/s ;  acfA = 0.590 m/s2 ;
d = 1.838 m ;  vrBA = 1.142 m/s ;
angBA = 164.452 grados ;  nA = 1.611 rev ;
nB = 1.146 rev ;  angfB = 52.530 grados ;
rfB = 0.960 m ;  vafB = 0.600 rad/s ;
vrfB = 0.080 m/s ;
Estado = success.

@end example
