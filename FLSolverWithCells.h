/* 
   Copyright (C) 2012, 2013, 2015, 2020 German A. Arias

   This file is part of FísicaLab application

   FísicaLab is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
 
   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
 
   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/

#import "FLSolver.h"

#ifdef MACOS
#import <GNUstep.h>
#endif

@interface FLSolverWithCells : FLSolver
{
  NSUInteger chalkboardWidth, chalkboardHeight;
  NSArray *cells;
}
- (void) setCells: (NSArray *)anArray;
- (NSArray *) cells;
- (void) withChalkboardWidth: (NSUInteger)aWidth height: (NSUInteger)anHeight;
- (NSUInteger) chalkboardWidth;
- (NSUInteger) chalkboardHeight;
- (void) searchAppliedElementsAt: (NSNumber *)identifier
                      andStoreIn: (NSMutableArray *)anArray;
- (void) searchAppliedElementsAt: (NSNumber *)identifier
		      andStoreIn: (NSMutableArray *)anArray
                  withPostionsIn: (NSMutableArray *)posArray;
@end
