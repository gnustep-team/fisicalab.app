/* 
   Copyright (C) 2009, 2010, 2012, 2013, 2015, 2016 German A. Arias

   This file is part of FísicaLab application

   FísicaLab is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
 
   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
 
   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/

#import <AppKit/AppKit.h>

@interface ChalkboardView : NSView
{
  // Outlet
  id information;

  // Instance variables
  int objectCode, count;
  NSUInteger width, height;
  BOOL newObject;
  BOOL moveObject;
  NSString *objects;
  NSImage *image;
  NSBezierPath *selectedCell;
}
- (void) addObject: (id)sender;
- (void) calculate: (id)sender;
- (void) clean: (id)sender;
- (void)clickCell: (id)sender;
- (NSString *) module;
- (NSUInteger) chalkboardWidth;
- (NSUInteger) chalkboardHeight;
- (NSArray *) cellsInfo;
- (void) controlCursor: (id)sender;

@end
