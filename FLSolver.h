/* 
   Copyright (C) 2012, 2013, 2015, 2016, 2017, 2020 German A. Arias

   This file is part of FísicaLab application

   FísicaLab is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
 
   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
 
   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/

#import <Foundation/Foundation.h>

#ifdef MACOS
#import <GNUstep.h>
#endif

@interface FLSolver : NSObject
{
  // Instance variables
  NSUInteger system;
  NSDictionary *conversions;
  id viewer;

  //Retained
  NSCharacterSet *numbers;
  NSArray *messagesArray;
}
+ (NSArray *) dataForElementWithTag: (NSUInteger)tag forSystem: (NSUInteger)sys;
- (void) solveProblemWithData: (NSMutableDictionary *)list;
- (void) setSystem: (NSUInteger)aNumber;
- (void) setConversions: (NSDictionary *)aDictionary;
- (void) setViewer: (id)aView;
- (void) setMessagesFile: (NSString *)fileName;
- (void) writeMessage: (NSString *)data;
- (void) writeMessageAtIndex: (NSUInteger)index;
- (void) writeMessageAtIndex: (NSUInteger)index
               withParameter: (NSString *)aString;
//- (NSTextView *) viewer;
- (BOOL) hasConversionTheString: (NSString *)data;
- (double) conversionFactorFor: (NSString *)info;
- (BOOL) isNumericDataTheString: (NSString *)data;
- (BOOL) isEmptyTheName: (NSString *)name;
- (NSString *) scientificNotationFor: (double)aNumber;
- (void) reduceAngleAt: (NSUInteger)index in:(NSMutableArray *)anArray;
- (void) fixVectorMagnitudeAt: (NSUInteger)vIndex
                  withAngleAt: (NSUInteger)angIndex
                           in: (NSMutableArray *)anArray;
- (void) fixForceMagnitudeAt: (NSUInteger)fIndex
                 withAngleAt: (NSUInteger)angIndex
                          in: (NSMutableArray *)anArray;
- (BOOL) searchUnknownsIn: (NSDictionary *)data
               andStoreIn: (NSMutableArray *)unknowns;
- (void) printUnknowns: (NSArray *)unknowns withResults: (NSArray *)results;
- (void) printUnknowns: (NSArray *)unknowns withResults: (NSArray *)results
            withStatus: (NSArray *)status;
@end
