/* 
   Copyright (C) 2020 German A. Arias

   This file is part of FísicaLab application

   FísicaLab is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
 
   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
 
   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/

#import <Foundation/Foundation.h>

@interface FLProcessMathematics : NSObject
{
  NSCharacterSet *numbers, *plus_minusSet, *multiplication_divisionSet;
}

- (NSString *) processEntry: (NSString *)anEntry;
- (NSString *) processAngleEntry: (NSString *)anEntry;
- (NSString *) operatePolynomial: (NSString *)anEntry;
- (NSString *) operateTerm: (NSString *)anEntry;
- (NSString *) operateFunction: (NSString *)anEntry;
- (BOOL) isNumericArray: (NSArray *)anArray;
- (BOOL) isNumeric: (NSString *)anEntry;

@end
