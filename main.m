/* 
   Project: FísicaLab

   Copyright (C) 2009, 2010, 2017, 2020

   Author: German A. Arias <germanandre@gmx.es>
   
   Created: 2008-09-10 18:56:00 -0600 by german

   This application is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public
   License as published by the Free Software Foundation; either
   version 3 of the License, or (at your option) any later version.
 
   This application is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.
 
   You should have received a copy of the GNU General Public
   License along with this library; if not, write to the Free
   Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111 USA.
*/

#ifdef IUPUI
#import <Foundation/Foundation.h>
#import "IupInterface.h"
#endif

#import <AppKit/AppKit.h>

#ifdef RENUI

#import "Renaissance/Renaissance.h"
#import "RenaissanceController.h"

int (*linkRenaissanceIn)(int, const char **) = GSMarkupApplicationMain; 

#endif

int 
main(int argc, const char *argv[])
{
#ifdef IUPUI
   IupInterface *interface;
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  // Init IUP interface.
  interface = [[IupInterface alloc] initInterface];

  [interface release];
  [pool release];

  return 0;
#else

#ifdef RENUI

  // Renaissance interface.
  NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

  [NSApplication sharedApplication];
  [NSApp setDelegate: [RenaissanceController new]];

  #ifdef MACOS
  [NSBundle loadGSMarkupNamed: @"menu" owner: [NSApp delegate]];
  #else
  [NSBundle loadGSMarkupNamed: @"menu-gs" owner: [NSApp delegate]];
  #endif
  
  [NSApp run];
  [pool release];

  return 0;
  
#else
  return NSApplicationMain (argc, argv);
#endif

#endif
}

